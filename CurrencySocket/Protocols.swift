//
//  Protocols.swift
//  CurrencySocket
//
//  Created by Владимир Елизаров on 10.07.17.
//  Copyright © 2017 Владимир Елизаров. All rights reserved.
//

import Foundation
import RxSwift




///    Implement Network Currency Service Protocol for object
///    which shoul be responseble to communicate with endpoint
///    for getting data
protocol NetworkCurrencyServiceProtocol {
    
    /// calls to get observable of loading data
    func subscribe(forTools tools: [String]) -> Observable<[Tick]>
    
    /// appends currency pair to get ticks of it
    func appendSubsribe(tool:String)
    
    /// remove currency pair
    /// after calls this method endpoint stops to send 
    /// data abount the currency pair
    func unsubscribe(forTool tool: String)
}



/// Protocol describes object which response to store/restore data
/// and user presets
protocol PersistentCurrencyServiceProtocol: CurrencyLocalServiceProtocol {
    
    func cacheUserTools(_ tools:[String])
    func cacheLastTicks(_ ticks:[Tick])
    func restoreUserTools() -> [String]?
    func restoreLastTicks() -> Observable<[Tick]>
}





