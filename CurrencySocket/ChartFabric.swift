//
//  ChartFabric.swift
//  CurrencySocket
//
//  Created by Владимир Елизаров on 10.07.17.
//  Copyright © 2017 Владимир Елизаров. All rights reserved.
//

import UIKit
import RxSwift

class ChartFabric {
    static func makeChartController(withDataObservable observable: Observable<Tick>) -> UIViewController {
        return ChartViewController.makeChartController(observable: observable)
    }
}
