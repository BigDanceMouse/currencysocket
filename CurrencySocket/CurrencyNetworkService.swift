//
//  CurrencyNetworkService.swift
//  CurrencySocket
//
//  Created by Владимир Елизаров on 09.07.17.
//  Copyright © 2017 Владимир Елизаров. All rights reserved.
//

import Foundation
import SwiftyJSON
import RxSwift


protocol WebSocketAdapter {
    
    static func makeNew(url: URL) -> WebSocketAdapter
    
    var onConnect: ((Void) -> Void)? { get set }
    var onDisconnect: ((NSError?) -> Void)? { get set }
    var onText: ((String) -> Void)? { get set }
    
    var isConnected: Bool { get }
    
    func connect()
    func write(string: String, completion: (() -> ())?)
}




fileprivate let kEndpointLink = "wss://quotes.exness.com:18400"
fileprivate let kSubscribeCommand = "SUBSCRIBE: "
fileprivate let kUnsubscribeCommand = "UNSUBSCRIBE: "


fileprivate enum SocketState: Int {
    case notConnected, ready
}


class CurrencyNetworkService: NetworkCurrencyServiceProtocol {

    private lazy var observer = PublishSubject<[Tick]>()
    private lazy var bag = DisposeBag()
    
    private var adapter: WebSocketAdapter
    
    private var marker = BehaviorSubject<SocketState>(value: .notConnected)
    
    
    init(withSocketAdapter: WebSocketAdapter.Type) {
        self.adapter = withSocketAdapter.makeNew(url: URL(string: kEndpointLink)!)

        setupAdapter()
        adapter.connect()
    }
    
    
    private func setupAdapter() {
        print(#function)
        
        adapter.onConnect = {[weak self] in
            self?.marker.onNext(.ready)
        }
        
        adapter.onDisconnect = {[weak self] err in
            print("disconnected")
            if let _err = err {
                print("********** ERROR **********")
                print(_err.localizedDescription)
            }
            self?.marker.onError(NetworkError.invalidConnection)
        }
        
        adapter.onText = {[weak self] text in
            print(text)
            if let dataFromString = text.data(using: .utf8, allowLossyConversion: false) {
                self?.handle(response: dataFromString)
            }
        }
    }
    
    private func handle(response: Data) {
        if let ticks = self.mapResponse(response)
        {
            self.observer.on(.next(ticks))
        }
        else {
            self.observer.on(.error(NetworkError.invalidResponseFormat))
            print("error whill and mapping")
        }
    }
    
    
    private func mapResponse(_ response: Data ) -> [Tick]? {
        let json = JSON(string: response)
        let list = json["subscribed_list"].exists()
            ? json["subscribed_list"]
            : json
        
        let ticks = list["ticks"].array
        
        return ticks?.flatMap(makeTick)
    }
    
    
    private func makeTick(from part: JSON) -> Tick? {
        if let pair = part["s"].string,
            let bid = Float(part["b"].stringValue),
            let ask = Float(part["a"].stringValue),
            let spred = Float(part["spr"].stringValue) {
            return Tick(pair: pair, bid: bid, ask: ask, spred: spred)
        }
        else {
            return nil
        }
    }
    
    
    func subscribe(forTools tools: [String]) -> Observable<[Tick]> {
        
        clearPreviuseSubscribes()
        
        marker.asObservable()
            .observeOn(ConcurrentDispatchQueueScheduler(queue: .global()))
            .filter { $0.hashValue == SocketState.ready.hashValue }
            .subscribe(onNext: {[weak self] _ in
                
                let request = kSubscribeCommand + tools.joined(separator: ",")
                
                self?.adapter.write(
                    string: request,
                    completion: nil
                )
            })
            .addDisposableTo(self.bag)
        
        
        return self.observer
    }
    
    
    private func clearPreviuseSubscribes() {
        self.bag = DisposeBag()
    }
    
    
    func appendSubsribe(tool:String) {
        let request = kSubscribeCommand + tool
        
        adapter.write(
            string: request,
            completion: nil
        )
    }
    
    func unsubscribe(forTool tool: String) {
        
        let request = kUnsubscribeCommand + tool
        
        adapter.write(
            string: request,
            completion: nil
        )
    }

    
    
}
