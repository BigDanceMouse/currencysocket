//
//  NSLayoutConstraint+CurrencySocket.swift
//  CurrencySocket
//
//  Created by Владимир Елизаров on 10.07.17.
//  Copyright © 2017 Владимир Елизаров. All rights reserved.
//

import UIKit



/// convinience creating constraint with visula constaint language
///
/// using:
///
/// let maker = constraint(["someView":someView])
/// self.view.addConstraints(make("V:|-[someView]-|"))

func constraint(_ views:[String : AnyObject])->(String)->[NSLayoutConstraint] {
    return {
        NSLayoutConstraint.constraints(withVisualFormat: $0, options: [], metrics: nil, views: views) }
}

