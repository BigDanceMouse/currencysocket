//
//  Operators.swift
//  CurrencySocket
//
//  Created by Владимир Елизаров on 10.07.17.
//  Copyright © 2017 Владимир Елизаров. All rights reserved.
//

import Foundation


precedencegroup BindPrecedence {
    associativity: left
    higherThan: MultiplicationPrecedence
    assignment: true
}


infix operator >>= :BindPrecedence

@_transparent
@discardableResult
public func >>=<T,U>(val:T?, f:((T)->U?)?)->U? {
    
    guard
        let just = val,
        let _f = f
        else { return nil }
    return _f(just)
}
