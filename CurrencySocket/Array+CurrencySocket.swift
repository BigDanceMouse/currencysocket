//
//  Array+CurrencySocket.swift
//  CurrencySocket
//
//  Created by Владимир Елизаров on 10.07.17.
//  Copyright © 2017 Владимир Елизаров. All rights reserved.
//

import Foundation

extension Array {
    subscript(safe index:Index)->Element? {
        get {
            return (startIndex..<endIndex) ~= index ? self[index] : nil
        }}
}
