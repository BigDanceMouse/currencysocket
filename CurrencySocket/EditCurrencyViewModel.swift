//
//  EditCurrencyViewModel.swift
//  CurrencySocket
//
//  Created by Владимир Елизаров on 09.07.17.
//  Copyright © 2017 Владимир Елизаров. All rights reserved.
//

import Foundation

extension EditCurrencyViewModel {
    
    static func makeDefault(nowSelectedTools tools: [String]) -> EditCurrencyViewModel {
        let service = CurrencyPersistentService()
        let viewModel = EditCurrencyViewModel(service: service, userSelectedCurrency: tools)
        return viewModel
    }
}



class EditCurrencyViewModel {
    
    private let service: CurrencyLocalServiceProtocol
    private var selectedList:[String: Bool] = [:]
    
    
    var totalCarrencyCount: Int {
        return selectedList.count
    }
    
    
    
    init(service: CurrencyLocalServiceProtocol, userSelectedCurrency: [String]) {
        self.service = service
        self.populateSelectedList(withUserSelecetd: userSelectedCurrency)
    }
    
    
    private func populateSelectedList(withUserSelecetd userSelectedCurrency: [String]) {
        for tool in service.availabelTools{
            selectedList[tool] = userSelectedCurrency.contains(tool)
        }
    }

    
    func pair(at index: Int) -> String {
        return service.availabelTools[index]
    }
    
    
    func isInUserList(pairName: String) -> Bool {
        return selectedList[pairName] ?? false
    }
    
    
    func setPairState(isInUserList: Bool, pairName: String) {
        selectedList[pairName] = isInUserList
    }
    
}
