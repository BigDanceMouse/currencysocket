//
//  CurrencyListViewController.swift
//  
//
//  Created by Владимир Елизаров on 10.07.17.
//
//

import UIKit
import RxSwift
import RxCocoa
import NSObject_Rx


infix operator >>= :BindPrecedence

fileprivate let kCurrencyCellReuseId = "CurrencyPairTableViewCell"

final class CurrencyListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private lazy var viewModel: CurrencyListViewModel = .makeDefault()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViewModle()
        setupTableView()
        setupSubscribers()
    }
    
    
    private func setupViewModle() {
        
        // bind selected cases to table view as data source
        viewModel.userSelectedPairs
            .asObservable()
            .bind(to: tableView.rx.items) {[weak self] tableView, row, _ in
                let cell = tableView.dequeueReusableCell(
                    withIdentifier: kCurrencyCellReuseId,
                    for: IndexPath(row: row, section: 0))
                    as! CurrencyPairTableViewCell
                
                self?.setup(currencyCell: cell, at: row)
                
                return cell
            }
            .addDisposableTo(self.rx_disposeBag)
    }
    
    
    private func setupTableView() {
        
        // append reaction for deliting row
        tableView.rx.itemDeleted
            .subscribe(onNext: {[weak self] in
                self?.viewModel.removePair(
                    at: $0.row
                )
            })
            .addDisposableTo(self.rx_disposeBag)
        
        
        // append reaction to move row to new position
        tableView.rx.itemMoved
            .subscribe(onNext: {[weak self] in
                self?.viewModel.movePair(
                    from: $0.sourceIndex.row,
                    to: $0.destinationIndex.row
                )
            })
            .addDisposableTo(self.rx_disposeBag)
        
        
        // when user will select some cell will be presented chart controller
        tableView.rx.itemSelected
            .subscribe(onNext: {[weak self] in
                
                self?.viewModel.tickObserverForPair(at: $0.row)?
                    .asObservable()
                    >>= ChartFabric.makeChartController
                    >>= self?.navigationController?.push
                
            })
            .addDisposableTo(self.rx_disposeBag)
    }
    
    
    /// subscribe to enterBackground notification
    /// to seve curent data and presets
    private func setupSubscribers() {
        
        NotificationCenter.default.rx
            .notification(NSNotification.Name.UIApplicationDidEnterBackground)
            .subscribe(onNext: {[weak self] _ in
                self?.viewModel.cacheData()
            })
            .addDisposableTo(self.rx_disposeBag)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    
    
    private func setup(currencyCell cell:CurrencyPairTableViewCell, at index: Int) {

        /// geting observer wich emits ticks for appropriate cell index
        
        if let observer = viewModel.tickObserverForPair(at: index) {
            
            /// cell will be setuped by each tick
            
            observer.asObservable()
                .subscribeOn(MainScheduler.instance)
                .subscribe(onNext: {[weak cell] tick in
                    cell?.currencyPairNameLabel.text = tick.pair.currencyPairDescriptionTitle
                    
                    /// don't shows zero value instead set text to double dash
                    let bidText = tick.bid != 0 ? String(tick.bid) : "——"
                    let askText = tick.ask != 0 ? String(tick.ask) : "——"
                    let spredText = tick.spred != 0 ? String(tick.spred) : "——"
                    cell?.bidAskLabel.text = bidText + "/" + askText
                    cell?.spredLabel.text = spredText
                })
                .addDisposableTo(cell.bag)
        }
    }
    

    
    @IBAction func openEditController(_ sender: Any) {
        
        let controller = EditCurrencyTableViewController
            .makeEditController(
                nowSelected: viewModel.userSelectedPairs.value
            )
        
        // when user toggle pair, controller.toggleObserver
        // emits tuple with pair name and toggle state.
        // transfer it to view model
        controller
            .toggleObserver
            .subscribe(onNext: {[weak self] in
                self?.viewModel.setPair($0.0, isInList:$0.1)
            })
            .addDisposableTo(controller.rx_disposeBag)
        
        self.navigationController?.push(viewController: controller)
    }
    
    
    @IBAction func editCellOrder(_ sender: UIBarButtonItem) {
        sender.title = tableView.isEditing ? "Настроить" : "Готово"
        self.tableView.setEditing(!tableView.isEditing, animated: true)
    }
    
    
}
