//
//  ChartViewController.swift
//  CurrencySocket
//
//  Created by Владимир Елизаров on 10.07.17.
//  Copyright © 2017 Владимир Елизаров. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Charts
import NSObject_Rx


fileprivate let kBidDataIndex = 0
fileprivate let kAskDataIndex = 1

extension ChartViewController {
    
    static func makeChartController(observable: Observable<Tick>) -> ChartViewController {
        
        let controller = UIStoryboard(
            name: "Main",
            bundle: nil)
            .instantiateViewController(
                withIdentifier: "ChartViewController")
            as! ChartViewController
        
        observable
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {
                print($0)
                controller.append(bid: $0.bid, ask: $0.ask)
            })
            .addDisposableTo(controller.rx_disposeBag)
        
        observable
            .take(1)
            .subscribe(onNext: { controller.title = $0.pair.currencyPairDescriptionTitle })
            .addDisposableTo(controller.rx_disposeBag)
        
        return controller
    }
}




final class ChartViewController: UIViewController {

    
    private weak var lineChartView: LineChartView?
    private var step:Double = 0


    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupChartView()
        setupDataLayers()
    }
    
    private func setupChartView() {
        let _lineChartView = LineChartView(frame: self.view.bounds)
        _lineChartView.scaleYEnabled = false
        _lineChartView.translatesAutoresizingMaskIntoConstraints = false
        _lineChartView.minOffset = 20
        _lineChartView.clipValuesToContentEnabled = true
        _lineChartView.extraTopOffset = 20
        _lineChartView.setVisibleXRange(minXRange: 1, maxXRange: 100)
        _lineChartView.chartDescription = nil
        _lineChartView.setDragOffsetX(30)
        
        
        self.view.addSubview(_lineChartView)
        self.lineChartView = _lineChartView
            
        let make = constraint(["chart":_lineChartView])
        view.addConstraints(make("V:|-0-[chart]-0-|"))
        view.addConstraints(make("H:|-0-[chart]-0-|"))
    }
    

    private func setupDataLayers() {
        
        let data = [
                (name:"BID", color: UIColor.red),
                (name:"ASK", color: UIColor.blue)
            ].map(self.makeDataLayer)
        
        self.lineChartView?.data = LineChartData(dataSets: data)
    }
    
    
    private func makeDataLayer(name:String, color: UIColor) -> LineChartDataSet {
        let set = LineChartDataSet(values: [], label: name)
        set.mode = .linear
        set.circleRadius = 0
        set.setColor(color)
        set.drawValuesEnabled = false
        return set
    }
    
    
    func append(bid: Float, ask: Float) {
        
        if let _lineView = lineChartView {
            
            _lineView.data?.addEntry(ChartDataEntry(x:step, y:Double(bid)), dataSetIndex: kBidDataIndex)
            _lineView.data?.addEntry(ChartDataEntry(x:step, y:Double(ask)), dataSetIndex: kAskDataIndex)
            
            _lineView.notifyDataSetChanged()
            _lineView.setVisibleXRange(minXRange: 1, maxXRange: 100)
            
            if _lineView.hasNoDragOffset {
                _lineView.moveViewToX(step)
            }
            
            step += 1
        }
    }
    
    
}
