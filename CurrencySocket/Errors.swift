//
//  Errors.swift
//  CurrencySocket
//
//  Created by Владимир Елизаров on 10.07.17.
//  Copyright © 2017 Владимир Елизаров. All rights reserved.
//

import Foundation



enum PersistentServiceError: Error {
    /// error produced whill restoreation process
    case restorationError
}



enum NetworkError: Error {
    
    /// endpoint response with invalid format 
    /// or response format was unexpected changed
    case invalidResponseFormat
    
    /// invalid connection to endpoint
    case invalidConnection
}
