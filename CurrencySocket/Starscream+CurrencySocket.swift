//
//  Starscream+CurrencySocket.swift
//  CurrencySocket
//
//  Created by Владимир Елизаров on 09.07.17.
//  Copyright © 2017 Владимир Елизаров. All rights reserved.
//

import Foundation
import Starscream


extension WebSocket: WebSocketAdapter {
    static func makeNew(url: URL) -> WebSocketAdapter {
        return WebSocket(url: url)
    }
}


/// default websocket implementation

func defaultWebSocketAdapter() -> WebSocketAdapter.Type {
    return WebSocket.self as WebSocketAdapter.Type
}
