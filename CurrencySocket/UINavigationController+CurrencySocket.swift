//
//  UINavigationController+CurrencySocket.swift
//  CurrencySocket
//
//  Created by Владимир Елизаров on 10.07.17.
//  Copyright © 2017 Владимир Елизаров. All rights reserved.
//

import UIKit


extension UINavigationController {
    
    /// convenience method for pushViewController(_, animated:_)
    func push(viewController: UIViewController) {
        self.pushViewController(viewController, animated: true)
    }
}
