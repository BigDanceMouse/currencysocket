//
//  String+CurrencySocket.swift
//  CurrencySocket
//
//  Created by Владимир Елизаров on 10.07.17.
//  Copyright © 2017 Владимир Елизаров. All rights reserved.
//

import Foundation




extension String {
    
    /// returns the same string but with "/" symbol in the middle
    var currencyPairDescriptionTitle: String {
        
        if self.isEmpty {
            return "/"
        }
        let length = self.characters.count

        let mid = length / 2
        let midIndex = self.index(self.startIndex, offsetBy: mid)
        var result = self
        
        result.insert("/", at: midIndex)
        return result
    }
}
