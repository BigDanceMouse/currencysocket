//
//  CurrencyPersistentService.swift
//  CurrencySocket
//
//  Created by Владимир Елизаров on 09.07.17.
//  Copyright © 2017 Владимир Елизаров. All rights reserved.
//

import Foundation
import RxSwift
import SwiftyJSON

protocol CurrencyLocalServiceProtocol {
    var availabelTools: [String] { get }
}

fileprivate let defaultTools = [
    "EURUSD",
    "EURGBP",
    "USDJPY",
    "GBPUSD",
    "USDCHF",
    "USDCAD",
    "AUDUSD",
    "EURJPY",
    "EURCHF"
]


fileprivate let kUserToolsKey = "CurrencySocket.userToolsKey"
fileprivate let kLastTicksKey = "CurrencySocket.lastTicksKey"



class CurrencyPersistentService:PersistentCurrencyServiceProtocol {
    
    var availabelTools: [String] {
        return defaultTools
    }
    
    func cacheUserTools(_ tools:[String]) {
        UserDefaults.standard.set(tools, forKey: kUserToolsKey)
    }
    
    func cacheLastTicks(_ ticks:[Tick]) {
        
        let data = ticks.reduce([String:[String:Any]]()) {
            var acc = $0
            var data = [String:Any]()
            data["ask"] = $1.ask
            data["bid"] = $1.bid
            data["spred"] = $1.spred
            acc[$1.pair] = data
            return acc
        }
        UserDefaults.standard.set(data, forKey: kLastTicksKey)
    }
    
    func restoreUserTools() -> [String]? {
        
        if let restored = UserDefaults.standard.array(forKey: kUserToolsKey) as? [String] {
            return restored
        }
        return nil
    }
    
    func restoreLastTicks() -> Observable<[Tick]> {
        return Observable.create {observer in
            if let restored = UserDefaults.standard.dictionary(forKey: kLastTicksKey) as? [String:[String:Any]]
                {
                   let ticks = restored.reduce([Tick]()) {
                        let pairName = $1.key
                        var result = $0
                        if  let bid = $1.value["bid"] as? Float,
                            let ask = $1.value["ask"] as? Float,
                            let spred = $1.value["spred"] as? Float
                        {
                            let tick = Tick(pair: pairName, bid: bid, ask: ask, spred: spred)
                            result.append(tick)
                        }
                        return result
                    }
                    print(restored)
                observer.onNext(ticks)
            }
            else {
                observer.onError(PersistentServiceError.restorationError)
            }
            
            return Disposables.create()
        }
    }
}
