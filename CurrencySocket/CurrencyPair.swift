//
//  CurrencyPair.swift
//  CurrencySocket
//
//  Created by Владимир Елизаров on 10.07.17.
//  Copyright © 2017 Владимир Елизаров. All rights reserved.
//

import Foundation



struct Tick {
    var pair: String
    var bid: Float
    var ask: Float
    var spred: Float
    
    static func zero(for pair:String) -> Tick {
        return Tick(pair: pair, bid: 0, ask: 0, spred: 0)
    }
}
