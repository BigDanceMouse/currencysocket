//
//  CurrencyListViewModel.swift
//  CurrencySocket
//
//  Created by Владимир Елизаров on 10.07.17.
//  Copyright © 2017 Владимир Елизаров. All rights reserved.
//

import Foundation
import RxSwift
import NSObject_Rx



final class CurrencyListViewModel: NSObject {
    
    
    static func makeDefault() -> CurrencyListViewModel {
        let netService = CurrencyNetworkService(withSocketAdapter: defaultWebSocketAdapter())
        let persistentService = CurrencyPersistentService()
        
        return CurrencyListViewModel(
            withNetworkService: netService,
            persistentService: persistentService
        )
    }
    
    
    private(set) var userSelectedPairs: Variable<[String]> = Variable([])
    private var _userSelectedPairs:[String] {
        get { return userSelectedPairs.value }
        set { userSelectedPairs.value = newValue }
        }
    
    private var currencyObservers: [String: Variable<Tick>] = [:]
    
    private var loader: BehaviorSubject<[Tick]> = BehaviorSubject(value: [])
    
    private let networker: NetworkCurrencyServiceProtocol
    private let persistentService: PersistentCurrencyServiceProtocol
    
    
    var totalItemsCount: Int {
        return _userSelectedPairs.count
    }
    
    
    init(withNetworkService networker: NetworkCurrencyServiceProtocol, persistentService: PersistentCurrencyServiceProtocol) {
        
        self.networker = networker
        self.persistentService = persistentService
        
        super.init()
        
        restoreAndSubscribe()
    }
    
    private func restoreAndSubscribe() {
        DispatchQueue.global().async {
            self._userSelectedPairs = self.restoreUserTools()
            self.fillUserCases(withCases: self._userSelectedPairs)
            self.getTicks(to: self._userSelectedPairs)
        }
    }
    
    private func restoreUserTools() -> [String] {
        
        /// if nothing to restore fill user tools with all availibel values
        return persistentService.restoreUserTools() ?? persistentService.availabelTools
    }
    
    
    private func fillUserCases(withCases cases: [String]) {
        currencyObservers = [:]
        
        cases.forEach {
            currencyObservers[$0] = Variable(Tick(pair: $0, bid: 0, ask: 0, spred: 0))
        }
    }

    
    private func getTicks(to cases: [String]) {
        
        /// start loading ticks
        
        let observer = self.networker
            .subscribe(forTools: cases)
        
        
        /// try to restore last ticks
        /// if there is loaded ticks - ignore restored
        
        persistentService
            .restoreLastTicks()
            .takeUntil(observer)
            .take(1)
            .filter { !$0.isEmpty }
            .subscribe(onNext: {[weak self] in
                print("================= RESTORED ==================")
                self?.handle(response: $0)
            })
            .addDisposableTo(self.rx_disposeBag)
        
        
        /// handle loaded ticks
        observer
            .subscribe(onNext: {[weak self] in
                self?.handle(response: $0)
            })
            .addDisposableTo(self.rx_disposeBag)
    }
    
    
    
    func handle(response:[Tick]) {
        
        // if currency pair of appropriate tick contains in user tools - emit tick
        response.forEach { tick in
            let toolName = tick.pair
            currencyObservers[toolName]?.value = tick
        }
    }
    
    
    func tickObserverForPair(at index: Int) -> Variable<Tick>? {
        
        return _userSelectedPairs[safe:index]
            >>= { self.currencyObservers[$0] }
    }
    
    
    func setPair(_ pair: String, isInList:Bool) {
        if isInList {
            addCurrencyInUserList(pair)
        }
        else {
            removeCurrencyFromUserList(pair)
        }
    }
    
    
    private func addCurrencyInUserList(_ pair: String) {
        if _userSelectedPairs.contains(pair) {
            return
        }
        
        self.currencyObservers.updateValue(Variable(Tick.zero(for: pair)), forKey: pair)
        self.userSelectedPairs.value.append(pair)
        self.networker.appendSubsribe(tool: pair)
    }
    
    
    private func removeCurrencyFromUserList(_ pair: String) {
        
        self.currencyObservers.removeValue(forKey: pair)
        
        if let index = _userSelectedPairs.index(of: pair) {
            userSelectedPairs.value.remove(at: index)
            self.networker.unsubscribe(forTool: pair)
        }
    }
    
    func removePair(at index: Int) {
        let pairName = _userSelectedPairs[index]
        removeCurrencyFromUserList(pairName)
    }
    
    func movePair(from oldIndex: Int, to newIndex: Int) {
        if oldIndex == newIndex {
            print("invalid indexes")
            return
        }
        let pairName = _userSelectedPairs[oldIndex]
        userSelectedPairs.value.remove(at: oldIndex)
        userSelectedPairs.value.insert(pairName, at: newIndex)
    }
    
    func cacheData() {
        persistentService.cacheUserTools(_userSelectedPairs)
        let ticks = Array(currencyObservers.values.map { $0.value })
        persistentService.cacheLastTicks(ticks)
    }
    
}
