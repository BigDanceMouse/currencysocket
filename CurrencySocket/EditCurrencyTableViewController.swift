//
//  EditCurrencyTableViewController.swift
//  CurrencySocket
//
//  Created by Владимир Елизаров on 09.07.17.
//  Copyright © 2017 Владимир Елизаров. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import NSObject_Rx


fileprivate let kToggleCellId = "TogglePairTableViewCell"

extension EditCurrencyTableViewController {
    
    static func makeEditController(nowSelected: [String]) -> EditCurrencyTableViewController {
        
        let controller = UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(
                withIdentifier: "EditCurrencyTableViewController")
            as! EditCurrencyTableViewController
        
        controller.viewModel = EditCurrencyViewModel.makeDefault(nowSelectedTools: nowSelected)
        
        return controller
    }
}



class EditCurrencyTableViewController: UITableViewController {

    var toggleObserver: PublishSubject<(String,Bool)> = PublishSubject()
    
    fileprivate var viewModel: EditCurrencyViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.totalCarrencyCount
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kToggleCellId, for: indexPath) as! TogglePairTableViewCell
        
        let pair = viewModel.pair(at: indexPath.row)
        let isInUserList = viewModel.isInUserList(pairName: pair)
        
        cell.pairTitle.text = pair.currencyPairDescriptionTitle
        cell.toggle.isOn = isInUserList
        
        /// each time user toggle currency viewModle append/remove it from user selected
        /// and notify listeners about it
        cell.toggle.rx
            .isOn
            .subscribe(onNext: {[weak self] in
                
                self?.toggleObserver
                    .onNext((pair, $0))
                
                self?.viewModel?
                    .setPairState(isInUserList: $0, pairName: pair)
                
            }
        )
        .addDisposableTo(cell.rx_disposeBag)
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! TogglePairTableViewCell
        cell.toggle.isOn = !cell.toggle.isOn
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
